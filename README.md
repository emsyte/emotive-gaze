# Eye Gazing
Description: this project tracks the eye gaze in a video and determines if the eyes are looking at the screen (center facing) or elsewhere.

## Instructions
This project runs on [serverless](https://serverless.com/). First you'll need to make sure you NodeJS and Python installed.

Then, to install the project you can run:

```
pipenv install # Install the python requirements
npm install --dev # Install the node requirements
```

To start the test server, run:

```
pipenv run npm run offline
```

## Usage
This project runs as a micro-service in AWS Lambda. It's triggered with an event that has json that looks like the following:

```
{
    "video": "<s3-key>"
}
```

Or if a callback is required:

```
{
    "video": "<s3-key>",
    "callback": "<callback url>",
    "token": "<callback token>"
}
```

It will read the video from S3, process it frame-by-frame and produce json output that looks like this:

```
{
    "attention": {
        "looking_left": 0.4753246753246753,
        "looking_right": 0,
        "looking_center": 0.32727272727272727,
        "blinking": 0.07012987012987013,
        "unknown": 0.12727272727272726,
        "frames": 385
    }
}
```

Each label is associated with the percentage of time in the video that it was predominate (e.g. in the example above it calculated that the subject was looking to the left 47.5% of the time)

In the case of a callback, it will POST that JSON to the given callback URL along with the callback token. E.g:

```
{
    "token": "<callback token>",
    "attention": {
        "looking_left": 0.4753246753246753,
        "looking_right": 0,
        "looking_center": 0.32727272727272727,
        "blinking": 0.07012987012987013,
        "unknown": 0.12727272727272726,
        "frames": 385
    }
}
```

## Implementation
This is implemented as a micro service using the serverless framework.
It's uploaded as a core bundle and two layers, one for python requirements, and another for ffmpeg.

Due to the size restrictions of AWS Lambda, there are a number of steps that were taken to avoid too large of a deployment bundle.

1. The model data (in the `/data` directory) is uploaded to S3. When the service runs, it will download the model data.
2. Many of the unneeded files are stripped from the Python packages.
    - All the `tests/*` files are removed
    - Most of the `botocore/data/*` files are removed (note: some of the botocore data files are required for reading from S3. Those files are kept)
3. Everything except for the `ffmpeg` binary is removed from the ffmpeg layer (e.g. `ffprobe` is removed).

## Pre-trained model(dlib)
The facial landmark detector implemented inside dlib produces 68 (x, y)-coordinates that map to specific facial structures. 
These 68 point mappings were obtained by training a shape predictor on the labeled iBUG 300-W dataset(https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/)

## Accuray
-about 98%
-It will work well for high resolution images
-It work not perform well for low resolution and hazy images