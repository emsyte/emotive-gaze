import enum

import cv2
import numpy as np


class GazeDirection(enum.Enum):
    LEFT = 'looking_left'
    RIGHT = 'looking_right'
    CENTER = 'looking_center'
    BLINKING = 'blinking'
    UNKNOWN = 'unknown'


class Attention:
    def __init__(self, gaze):
        self._gaze = gaze
        self._attention = []
    
    @property
    def data(self):
        return list(self._attention)

    def process_frame(self, image):
        self._gaze.refresh(image)

        self._attention.append(
            self._get_frame_attention()
        )

    def process_frame_file(self, filename):
        image = cv2.imread(str(filename))
        self.process_frame(image)

    def _get_gaze_direction(self):
        if self._gaze.is_blinking():
            return GazeDirection.BLINKING
        elif self._gaze.is_right():
            return GazeDirection.RIGHT
        elif self._gaze.is_left():
            return GazeDirection.LEFT
        elif self._gaze.is_center():
            return GazeDirection.CENTER
        return GazeDirection.UNKNOWN
    
    def _get_frame_attention(self):
        if self._get_gaze_direction() == GazeDirection.CENTER:
            return 1
        return 0
