import json
import logging
import os
import tempfile
from contextlib import contextmanager
from pathlib import Path

import boto3
import botocore
import cv2
import numpy as np

from .attention import Attention
from .gaze_tracking import GazeTracking

logger = logging.getLogger("gaze_tracking")

OFFLINE = "IS_OFFLINE" in os.environ

MEDIA_BUCKET_NAME = "emsyte-media-production"
DATA_BUCKET_NAME = "gaze-tracking-data-production"

MODEL_NAME = "trained_models/shape_predictor_68_face_landmarks.dat"

if OFFLINE:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

s3 = boto3.client("s3")


def get_model():
    if OFFLINE:
        return open(
            os.path.join(os.path.dirname(os.path.dirname(__file__)), "data", MODEL_NAME)
        )

    download_path = os.path.join("/tmp/", MODEL_NAME)

    # Download the model file if it's not already downloaded
    if not os.path.exists(download_path):
        os.makedirs(os.path.dirname(download_path), exist_ok=True)

        with open(download_path, "wb") as file:
            s3.download_fileobj(DATA_BUCKET_NAME, MODEL_NAME, file)

    return download_path


model_name = get_model()
gaze = GazeTracking(model_name)


def handler(event, context):
    if OFFLINE:
        event = json.loads(event["body"])

    image = event["image"]

    logger.info("Processing image %s", image)

    with get_image(url=image) as image:
        attention = Attention(gaze)

        attention.process_frame_file(image.name)

        return {"success": True, "data": {"attention_score": attention.data[0]}}


@contextmanager
def get_image(url):
    if OFFLINE:
        yield open(url)
        return

    with download_file(url, MEDIA_BUCKET_NAME) as file:
        yield file


@contextmanager
def download_file(key, bucket):
    with tempfile.NamedTemporaryFile() as file:
        s3.download_fileobj(bucket, key, file)
        file.seek(0)
        yield file
