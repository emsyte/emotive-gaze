import json
import logging
import os
os.environ['IS_OFFLINE'] = 'True'

from gaze import handler

def test_attention():
    result = handler({
        'body': json.dumps({
            "image": os.path.join(os.path.dirname(__file__), '../4.jpg')
        })
    }, None)

    assert result.keys() == {'success', 'data'}
    assert result['success'] == True
    assert result['data'] == {
        'attention_score': 0
    }
